
$(document).ready(function(){

    $("#link_toComment").click(function(){
        $("#banner_comment").css("display","none");
        $("#form_comment").css("display","initial");
    });

    $(".form-group .form-control").click(function(){
        $('body').css("background-image", "url(./res/color.jpg)");
    });
});

function getData(){

    var nombre_comment = $('#nombre_comment').val();
    var comment = $('#comment').val();
    var selectedVal = $("#places option:selected").text();

    if(nombre_comment == null || nombre_comment.length == 0 || /^\s+$/.test(nombre_comment)){
        return alert('Ingrese nombre');
    }
    if(comment == null || comment.length == 0 || /^\s+$/.test(comment) || comment.length > 200){
        return alert('Ingrese comentario hasta 200 caracteres');
    }

    const commentTemplate = (nombreComment,coment,location) =>
        `<div class="col-lg-3">
            <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative uno">
                <div class="col p-4 d-flex flex-column position-static">
                    <strong class="d-inline-block mb-2 text-danger"> ${nombreComment} </strong>
                    <h3 class="star">★★★★★</h3>
                    <h5 class="mb-0"> ${coment} </h5>
                    <div class="mb-1 text-muted"> ${location} </div>
                    <p class="card-text mb-auto"></p>
                </div>
            </div>
        </div>`;
    
    $(commentTemplate(nombre_comment,comment,selectedVal)).replaceAll('#comments .col-lg-3:first');

}

function validateF(){

    var nombre_form = $('#exampleInputName1').val();
    var num_form = $('#exampleInputTel1').val();
    var email = $('#exampleInputEmail1').val();
    var hasFails = false;

    if( nombre_form == null || nombre_form.length == 0 || /^\s+$/.test(nombre_form) ) {
        $('#exampleInputName1').removeClass('is-valid').addClass('is-invalid');
        hasFails = true;
    } else {
        $('#exampleInputName1').removeClass('is-invalid').addClass('is-valid');
    }

    if( num_form == null || num_form.length == 0 || /^\s+$/.test(num_form) ) {
        $('#exampleInputTel1').removeClass('is-valid').addClass('is-invalid');
        hasFails = true;
    } else {
        $('#exampleInputTel1').removeClass('is-invalid').addClass('is-valid');
    }

    if( email == null || email.length == 0 || /^\s+$/.test(email) ) {
        $('#exampleInputEmail1').removeClass('is-valid').addClass('is-invalid');
        hasFails = true;
    } else {
        $('#exampleInputEmail1').removeClass('is-invalid').addClass('is-valid');
    }

    var condiciones = $("#exampleCheck1").is(":checked");
    if (!condiciones) {
        alert("Debe comprobar que eres humano!");
        hasFails = true;
    }

    if(hasFails){
        return false;
    } else {
        window.location.href = 'cover.html';
        return true;
    }
}

function volver(){
    window.location.href = 'index.html';
}







    /*
    const postsuscripcion = (nombre, email_u) =>
    `<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
        <main role="main" class="inner cover">
            <h1 class="cover-heading">Muchas gracias por suscribirte ${nombre} !</h1>
            <p class="lead">Pronto te estarémos enviando a ${email_u}: Noticias, Descuentos y Datos interesantes a tu email!</p>
            <p class="lead">Recuerda comentarselo a tu amigo viajero!</p>
            <button class="btn btn-danger" id="btn_volver" onclick="volver()">Volver</button>
        </main>
    </div>`;    
    
    $(postsuscripcion(nombre_form, email));*/

