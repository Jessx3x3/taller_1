# Taller_1

#Landing Page - Viajes

#Consta en una página web tipo "Landing Page" que trata de captar la atención del
#cliente y no distraerlo con demás información. Queremos que el usuario vea en la
#página solo información buena, beneficios a los que puede optar por suscribirse.
#Están a su vista los comentarios mejor evaluados y verídicos de personas que se
#han suscribido. El futuro cliente se sentirá en confianza y probablemente si 
#tiene ganas de viajar a EUA, pensará en suscribirse para obtener descuentos e 
#información relevante en su estadía futura. Si el cliente ya utilizó nuestros
#servicios, también, puede optar a comentar su experiencia y aparecer en las cajas
#de comentarios de la página. Éste comentario, sustituirá a los 4 máximos que
#aparecen en pantalla. 

#Como anteriormente se escribió, la idea no es saturar al cliente con información.

#Desarrollado con HTML, BOOTSTRAP, CSS, JS(QUERY).

